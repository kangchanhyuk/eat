package com.example.eat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ManagerActivity extends AppCompatActivity {

    Button managefood, managetable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);

        managefood = findViewById(R.id.button_food);
        managetable = findViewById(R.id.button_table);

        managefood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(ManagerActivity.this, ManageFoodActivity.class);
                startActivity(intent);

            }
        });
        managetable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =  new Intent(ManagerActivity.this, ManageTableActivity.class);
                startActivity(intent);

            }
        });


    }



}
