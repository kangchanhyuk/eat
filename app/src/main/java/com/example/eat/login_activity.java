package com.example.eat;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
/*
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.widget.Toast;
*/


public class login_activity extends AppCompatActivity {

    EditText id, password;
    Button login, manager;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_activity);

        id = (EditText)findViewById(R.id.edittext_id);
        password= (EditText)findViewById(R.id.edittext_password);
        login = findViewById(R.id.button_login);
        manager = findViewById(R.id.button_manager);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                String id = getText().toString();
                String pw = getText().toString();
                if(id == "id" && pw == "1234");  */
                intent =  new Intent(login_activity.this, UserActivity.class);
                startActivity(intent);
                finish();
            }
        });
        manager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent =  new Intent(login_activity.this, ManagerActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}

/*
public class MessageDemo extends Activity implements View.OnClickListener{
    Button alert; //팝업버튼선언
@Override
public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.main); //팝업버튼 설정
    alert=(Button)findViewById(R.id.id); //R.id.alert는 팝업버튼 아이디
    alert.setOnClickListener(this);
}

public void onClick(View view){
    if(view!=alert){ //view가 alert 이면 팝업실행 즉 버튼을 누르면 팝업창이 뜨는 조건
         Context mContext = getApplicationContext();
         LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);

         //R.layout.dialog는 xml 파일명이고 R.id.popup은 보여줄 레이아웃 아이디
        View layout = inflater.inflate(R.layout.dialog,(ViewGroup) findViewById(R.id.popup));
        AlertDialog.Builder aDialog = new AlertDialog.Builder(CustomActivity.this);
            aDialog.setTitle("LOGIN FAILED"); //타이틀바 제목
            aDialog.setView(layout); //dialog.xml 파일을 뷰로 셋팅
        // 그냥 닫기버튼을 위한 부분
        Dialog.setNegativeButton("닫기", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        //팝업창 생성
        AlertDialog ad = aDialog.create();
        ad.show();//보여줌!

    }
        */



