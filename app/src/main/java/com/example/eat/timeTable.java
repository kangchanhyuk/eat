package com.example.eat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class timeTable extends AppCompatActivity {
    static final String[] timeList= {"12:00~13:00","14:00~15:00","18:00~19:00","20:00~21:00"};
    Intent intent2;
    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,timeList);
        ListView listview = (ListView) findViewById(R.id.listview1) ;
        listview.setAdapter(adapter);

    }
}

