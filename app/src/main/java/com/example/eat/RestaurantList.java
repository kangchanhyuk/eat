package com.example.eat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.lang.String;

public class RestaurantList extends AppCompatActivity {
    static final String[] Restaurantlist= {"맥도날드","롯데리아","버거킹","맘스터치","맛이짱","미스터쉐프"};

    Intent intent2;
    ArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);
        adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,Restaurantlist);
        ListView listview = (ListView) findViewById(R.id.listview1) ;
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                intent2 =  new Intent(RestaurantList.this,numberPeople.class);
                startActivity(intent2);
            }

        });

    }
}
